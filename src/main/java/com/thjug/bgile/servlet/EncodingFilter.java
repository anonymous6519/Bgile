/*
 * Attribution
 * CC BY
 * This license lets others distribute, remix, tweak,
 * and build upon your work, even commercially,
 * as long as they credit you for the original creation.
 * This is the most accommodating of licenses offered.
 * Recommended for maximum dissemination and use of licensed materials.
 *
 * http://creativecommons.org/licenses/by/3.0/
 * http://creativecommons.org/licenses/by/3.0/legalcode
 */
package com.thjug.bgile.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.google.inject.Singleton;

/**
 *
 * @author Wasan Anusornhirunkarn, @tone
 */
@Singleton
public class EncodingFilter implements Filter {
	private static String ENCODING = "UTF-8";

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		final String encodingParam = filterConfig.getInitParameter("encoding");
		if (encodingParam != null) {
			ENCODING = encodingParam;
		}
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
			throws IOException, ServletException {
		request.setCharacterEncoding(ENCODING);
		filterChain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}
